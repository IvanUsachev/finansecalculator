package com.example.maximus.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

import butterknife.ButterKnife;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {

    EditText sumDeposit, persentYear, timeMonth, plusDeposit, persenttax;
    CheckBox capital;
    TextView monthMoney, totalMoney, totalPlusDeposit, totalMoneyPlus;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(MainActivity.this);

        sumDeposit = (EditText) findViewById(R.id.contribution_sum);
        persentYear = (EditText) findViewById(R.id.rate_percent);
        timeMonth = (EditText) findViewById(R.id.amount_month);
        plusDeposit = (EditText) findViewById(R.id.refill_month);
        persenttax = (EditText) findViewById(R.id.tax_percent);
        capital = (CheckBox) findViewById(R.id.checkbox_view);
        monthMoney = (TextView) findViewById(R.id.value_egemesachn_dohod);
        totalMoney = (TextView) findViewById(R.id.value_all_dohod);
        totalPlusDeposit = (TextView) findViewById(R.id.value_all_sum);
        totalMoneyPlus = (TextView) findViewById(R.id.value__all_result);


        capital.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    DecimalFormat two = new DecimalFormat("#.##");
                    int valuepercentYear = Integer.parseInt(getValue(persentYear));
                    int valuepersentTax = Integer.parseInt(getValue(persenttax));
                    int valuesumDeposit = Integer.parseInt(getValue(sumDeposit));
                    int valuetimemonth = Integer.parseInt(getValue(timeMonth));
                    int valuePlusDepost = Integer.parseInt(getValue(plusDeposit));


                    double valuePersent = (double) valuepercentYear / (double) 100;
                    double valueTax = (double) valuepersentTax / (double) 100;
                    double valuePersentWithTax = (valuePersent - valueTax) / 12 + 1;
                    double capitaliz = (valuesumDeposit * Math.pow(valuePersentWithTax, valuetimemonth)) - valuesumDeposit;
                    double valueMonthMoney = (valuesumDeposit * (valuePersent - valueTax)) / 12;
                    double valueTotalMoney = valueMonthMoney * valuetimemonth + capitaliz;
                    double valueTotalPlus = valuetimemonth * valuePlusDepost;
                    double valueTotalSum = valueTotalMoney + valuesumDeposit + valueTotalPlus + capitaliz;

                    monthMoney.setText(String.valueOf(two.format(valueMonthMoney)));
                    totalMoney.setText(String.valueOf(two.format(valueTotalMoney)));
                    totalPlusDeposit.setText(String.valueOf(two.format(valueTotalPlus)));
                    totalMoneyPlus.setText(String.valueOf(two.format(valueTotalSum)));

                } else {
                    DecimalFormat two = new DecimalFormat("#.##");
                    int valuepercentYear = Integer.parseInt(getValue(persentYear));
                    int valuepersentTax = Integer.parseInt(getValue(persenttax));
                    int valuesumDeposit = Integer.parseInt(getValue(sumDeposit));
                    int valuetimemonth = Integer.parseInt(getValue(timeMonth));
                    int valuePlusDepost = Integer.parseInt(getValue(plusDeposit));


                    double valuePersent = (double) valuepercentYear / (double) 100;
                    double valueTax = (double) valuepersentTax / (double) 100;
                    double valueMonthMoney = (valuesumDeposit * (valuePersent - valueTax)) / 12;
                    double valueTotalMoney = valueMonthMoney * valuetimemonth;
                    double valueTotalPlus = valuetimemonth * valuePlusDepost;
                    double valueTotalSum = valueTotalMoney + valuesumDeposit + valueTotalPlus;

                    monthMoney.setText(String.valueOf(two.format(valueMonthMoney)));
                    totalMoney.setText(String.valueOf(two.format(valueTotalMoney)));
                    totalPlusDeposit.setText(String.valueOf(two.format(valueTotalPlus)));
                    totalMoneyPlus.setText(String.valueOf(two.format(valueTotalSum)));
                }
            }
        });
    }


    public String getValue(EditText text) {
        if (text.getText().toString().isEmpty()) {
            return "0";
        } else {
            return text.getText().toString();
        }
    }


}
